//
//  DataProvider.swift
//  QardioTestAssignmentDataSource
//
//  Created by Dmitrii on 13/12/2017.
//  Copyright © 2017 Qardio. All rights reserved.
//

import Foundation

class DataProvider {

    private let timer: DispatchSourceTimer
    private var listeners = [DataProviderListener]()

    init() {
        timer = DispatchSource.makeTimerSource()
        timer.schedule(deadline: .now(), repeating: .milliseconds(2))
        timer.setEventHandler(handler: {
            let measurememnt = arc4random_uniform(181) + 40
            for listener in self.listeners {
                listener.measurementUpdated(measurememnt)
            }
        })
        timer.resume()
    }

    func subscribeNewListener(_ listener: DataProviderListener) {
        listeners.append(listener)
    }
}

protocol DataProviderListener {
    func measurementUpdated(_ measurement: UInt32)
}


