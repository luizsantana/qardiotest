//
//  AverageListener.swift
//  QardioTestAssignmentDataSource
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/2/18.
//  Copyright © 2018 DI. All rights reserved.
//

import Foundation

class AverageListener: DataProviderListener {
    
}

extension AverageListener {
    
    func measurementUpdated(_ measurement: UInt32) {
        
        DataController.getInstance().insertAverageHR(Int16(measurement))
        
        if (measurement < 65) {
            
            DataController.getInstance().insertAverageRHR(Int16(measurement))
        }
        
        DataController.getInstance().saveContext()
    }
}
