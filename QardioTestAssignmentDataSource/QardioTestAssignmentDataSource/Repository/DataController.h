//
//  DataController.h
//  QardioTestAssignmentDataSource
//
//  Created by Dmitrii on 13/12/2017.
//  Copyright © 2017 Qardio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataController : NSObject
@property (strong, nonatomic, readonly) NSManagedObjectContext* mainContext;
@property (strong, nonatomic, readonly) NSPersistentContainer* mainContainer;

+ (instancetype) getInstance;

- (void)initialise;
- (void)saveContext;

- (void) insertAverageHR:(int16_t)heartrate;
- (void) insertAverageRHR:(int16_t)heartrate;

- (void) getAverageCallback:(void(^)(double valueHR, double valueRHR))callback;
- (void) getMostFrequentHRCallback:(void(^)(int16_t valueHR))callback;

@end
