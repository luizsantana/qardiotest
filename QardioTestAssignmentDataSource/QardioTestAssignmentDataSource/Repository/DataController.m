//
//  DataController.m
//  QardioTestAssignmentDataSource
//
//  Created by Dmitrii on 13/12/2017.
//  Copyright © 2017 Qardio. All rights reserved.
//

#import "DataController.h"
#import "CoreData/CoreData.h"

#import "QardioTestAssignmentDataSource-Swift.h"

@interface DataController ()
@property (strong, nonatomic) NSPersistentContainer*    mainContainer;
@property (strong, nonatomic) NSManagedObjectContext*   mainContext;
@end

@implementation DataController

#pragma mark - Singleton

+ (instancetype) getInstance {
    
    static DataController *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DataController alloc] init];
    });
    
    return instance;
}

#pragma mark - Init

- (id)init
{
    self = [super init];
    if (!self) return nil;
    
    [self initialise];
    
    return self;
}

#pragma mark - Core Data Manage

- (void)initialise
{
    @synchronized (self)
    {
        if (self.mainContainer == nil)
        {
            self.mainContainer = [[NSPersistentContainer alloc] initWithName:@"QardioTestAssignmentDataSource"];
            
            [self.mainContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error)
             {
                 if (error != nil)
                 {
                     NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                     abort();
                 }
             }];
        }
        
        self.mainContext = self.mainContainer.viewContext;
    }
}

- (void)saveContext
{
    NSManagedObjectContext *context = self.mainContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - Core Data

- (void) insertAverageHR:(int16_t)heartrate {
    
    [self insertAverage:heartrate entity:@"AverageHR"];
}

- (void) insertAverageRHR:(int16_t)heartrate {
    
    [self insertAverage:heartrate entity:@"AverageRHR"];
}

- (void) insertAverage:(int16_t)heartrate entity:(NSString *) entity {
    
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    moc.persistentStoreCoordinator = self.mainContainer.persistentStoreCoordinator;
    [moc setStalenessInterval:1.0];
    [moc performBlockAndWait:^{
        
        NSDate *time = [NSDate date];
        
        Average *averageHR = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:moc];
        
        averageHR.heartrate = heartrate;
        averageHR.timestamp = time;
        
        NSError *error = nil;
        if ([moc save:&error] == NO) {
            NSAssert(NO, @"Error ：%s saving context: %@\n%@", __FUNCTION__,[error localizedDescription], [error userInfo]);
        }
    }];
}

- (void) getAverageCallback:(void(^)(double valueHR, double valueRHR))callback {
    
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    moc.persistentStoreCoordinator = self.mainContainer.persistentStoreCoordinator;
    [moc performBlock:^{
    
        NSFetchRequest *fetchRequestHR = [[NSFetchRequest alloc] initWithEntityName:@"AverageHR"];
        NSFetchRequest *fetchRequestRHR = [[NSFetchRequest alloc] initWithEntityName:@"AverageRHR"];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                                       ascending:NO];
        
        [fetchRequestHR setSortDescriptors:@[sortDescriptor]];
        fetchRequestHR .includesPendingChanges = YES;
        fetchRequestHR.fetchLimit = 50000;
        NSArray *resultHR = [moc executeFetchRequest:fetchRequestHR error:nil];
       
        NSNumber *averageHR = [resultHR valueForKeyPath:@"@avg.heartrate"];
        
        [fetchRequestRHR setSortDescriptors:@[sortDescriptor]];
        [fetchRequestRHR setIncludesPendingChanges:YES];
        [fetchRequestRHR setFetchLimit:1000];
        NSArray *resultRHR = [moc executeFetchRequest:fetchRequestRHR error:nil];
        NSNumber *averageRHR = [resultRHR valueForKeyPath:@"@avg.heartrate"];

        callback([averageHR doubleValue], [averageRHR doubleValue]);
    }];
}

- (void) getMostFrequentHRCallback:(void(^)(int16_t valueHR))callback {
    
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    moc.persistentStoreCoordinator = self.mainContainer.persistentStoreCoordinator;
    [moc performBlock:^{
        
        NSFetchRequest *fetchRequestHR = [[NSFetchRequest alloc] initWithEntityName:@"AverageHR"];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                                       ascending:NO];
        
        [fetchRequestHR setSortDescriptors:@[sortDescriptor]];
        fetchRequestHR .includesPendingChanges = YES;
        fetchRequestHR.fetchLimit = 20000;
        NSArray *resultHR = [moc executeFetchRequest:fetchRequestHR error:nil];
        
        NSCountedSet *valuesHRSet = [NSCountedSet set];
        for (Average *averageHR in resultHR) {
            
            [valuesHRSet addObject:[NSNumber numberWithShort:averageHR.heartrate]];
        }
        
        NSUInteger mostCommon = 0;
        NSNumber *mostValueHR = @0;
        for (id valueMost in valuesHRSet) {
            
            NSUInteger current = [valuesHRSet countForObject:valueMost];
            if (current > mostCommon) {
                
                mostCommon = current;
                mostValueHR = valueMost;
            }
        }

        callback([mostValueHR shortValue]);
    }];
}

@end
