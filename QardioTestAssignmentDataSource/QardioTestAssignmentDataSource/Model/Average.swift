//
//  Average.swift
//  QardioTestAssignmentDataSource
//
//  Created by Luiz Carlos Sant'Ana Junior on 20/2/18.
//  Copyright © 2018 DI. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class Average: NSManagedObject {
    
    @NSManaged var heartrate: Int16
    @NSManaged var timestamp: Date
}
