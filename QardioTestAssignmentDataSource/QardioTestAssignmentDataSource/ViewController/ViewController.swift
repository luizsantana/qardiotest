//
//  ViewController.swift
//  QardioTestAssignmentDataSource
//
//  Created by Dmitrii on 13/12/2017.
//  Copyright © 2017 Qardio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var timerSession: Timer!
    var timerAverage: Timer!
    var timerMostFrequent: Timer!
    
    var sessionDate: NSDate!
    
    @IBOutlet weak var averageHR: UILabel!
    @IBOutlet weak var averageRHR: UILabel!
    @IBOutlet weak var mostFrequentHR: UILabel!
    @IBOutlet weak var sessionTime: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sessionDate = NSDate()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.timerSession = Timer.scheduledTimer(withTimeInterval: 0.001, repeats:true, block: { (timerBlock: Timer) in
            
            DispatchQueue.main.async {
                
                self.updateSessionUI()
            }
        })
        
        self.timerAverage = Timer.scheduledTimer(withTimeInterval: 0.5, repeats:true, block: { (timerBlock: Timer) in
            
            self.updateAverageUI()
        })
        
        self.timerMostFrequent = Timer.scheduledTimer(withTimeInterval: 5.0, repeats:true, block: { (timerBlock: Timer) in
            
            self.updateMostFrequentUI()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> NSString {
        
        let ti = NSInteger(interval)
        
        let ms = Int((interval.truncatingRemainder(dividingBy: 1)) * 1000)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        return NSString(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
    }
    
    func updateSessionUI() {
        
        self.sessionTime.text = stringFromTimeInterval(interval: Date().timeIntervalSince(self.sessionDate as Date)) as String
    }
    
    func updateAverageUI() {
        
        DataController.getInstance().getAverageCallback { (valueHR: Double, valueRHR: Double) in
            
            DispatchQueue.main.async {
                
                self.averageHR.text = String(format: "%.2f", valueHR)
                self.averageRHR.text = String(format: "%.2f", valueRHR)
            }
        }
    }
    
    func updateMostFrequentUI() {
        
        DataController.getInstance().getMostFrequentHRCallback { (valueHR: Int16) in
            
            DispatchQueue.main.async {
                
                self.mostFrequentHR.text = String(format: "%i", valueHR)
            }
        }
    }
}

